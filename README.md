# Cross Country Time Keeper
![Cross Country Time Keeper](./Screenshots/Main%20Menu.jpg) 

## Description
This program was created back in 1998 when I was doing Cross Country.  This was written using QuickBasic 4.5.  It's purpose was to keep track of my running times on various courses.  This project is licensed under version 2 of the GPL.  You may view this license online [http://www.gnu.org/licenses/gpl-2.0.txt here].

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
Run the program.  On the main menu you have the following options:
 - Add:  Add a new record.
 - Search:  Search existing records by date, event or location.
 - Print: Display all records.  Also allows recorts to be printed.
 - Sort:  Sort all records by date, event or location.
 - Get top 10:  Display the top 10 records from a specific event.
 - Backup:  Backup the existing .dat file to C:\passw\BackupR.dat.
 - Exit: Quit the program.
 
All data will be saved to C:\PASSW\RUNNING.DAT.

## History
I believe this was started sometime in mid to late 1998, although I'm unsure of the exact date.

Based on the last modified time stamp, this project was last updated on 13 November 1998.

Download: [Binary](./bin/RunningExe.zip) | [Source](./bin/RunningSource.zip)
